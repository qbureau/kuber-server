package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/websocket"

	"time"
	"encoding/json"
	"kuber-server/commander"
	"html/template"
	"net"
)

var (
	addr = flag.String("addr", "0.0.0.0:8081", "http service address")
)

// func ping(ws *websocket.Conn, done chan struct{}) {
// 	ticker := time.NewTicker(pingPeriod)
// 	defer ticker.Stop()
// 	for {
// 		select {
// 		case <-ticker.C:
// 			if err := ws.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(writeWait)); err != nil {
// 				log.Println("ping:", err)
// 			}
// 		case <-done:
// 			return
// 		}
// 	}
// }

func internalError(ws *websocket.Conn, msg string, err error) {
	log.Println(msg, err)
	ws.WriteMessage(websocket.TextMessage, []byte("Internal server error."))
}

type Message struct {
	Command string `json:"command"`
	Body string `json:"body"`
	Done string `json:"done,omitempty"`
}

var upgrader = websocket.Upgrader{}

func serveWs(w http.ResponseWriter, r *http.Request) {
	var wsUpgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	ws, err := wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade:", err)
		return
	}

	defer ws.Close()

	for {
		messageType, b, err := ws.ReadMessage()
		log.Println("messageType", messageType)
		if err != nil {
			log.Println(err)
			return
		}
		originalCommand := string(b)
		cmd := strings.Fields(originalCommand)
		log.Println(string(b))

		go func(wss *websocket.Conn) {
			c := make(chan string)
			done := make(chan bool)
			go commander.RunScript(cmd, c, done)
			timeout := make(chan bool, 1)
			go func() {
				time.Sleep(3 * time.Second)
				timeout <- true
			}()
		loop:
			for {
				select {
				case res := <-c:
					{
						m := Message{originalCommand, res, ""}
						b, err := json.Marshal(m)
						if err != nil {
							log.Println(err)
							return
						}
						if err := wss.WriteMessage(messageType, b); err != nil {
							log.Println(err)
							return
						}
					}
				case <-done:
					m := Message{originalCommand, "done", "done"}
					b, err := json.Marshal(m)
					if err != nil {
						log.Println(err)
						return
					}
					if err := wss.WriteMessage(messageType, b); err != nil {
						log.Println(err)
						return
					}
					break loop
				case <-timeout:
					{
						m := Message{originalCommand, "timeout", "done"}
						b, err := json.Marshal(m)
						if err != nil {
							log.Println(err)
							return
						}
						if err := wss.WriteMessage(messageType, b); err != nil {
							log.Println(err)
							return
						}
						log.Println("timeout for ", cmd)
						break loop
					}
				}
			}
			log.Println("exiting")
		}(ws)
	}

}

func GetFQDN() string {
	hostname, err := os.Hostname()
	if err != nil {
		return "unknown"
	}

	addrs, err := net.LookupIP(hostname)
	if err != nil {
		return hostname
	}

	for _, addr := range addrs {
		if ipv4 := addr.To4(); ipv4 != nil {
			ip, err := ipv4.MarshalText()
			if err != nil {
				return hostname
			}
			hosts, err := net.LookupAddr(string(ip))
			if err != nil || len(hosts) == 0 {
				return hostname
			}
			fqdn := hosts[0]
			return strings.TrimSuffix(fqdn, ".") // return fqdn without trailing dot
		}
	}
	return hostname
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", 404)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", 405)
		return
	}
	//http.ServeFile(w, r, "home.html")
	tmpl, _ := template.ParseFiles("home.html")
	var data = struct{
		Ip string
		Dns string
		Hostname string
		AdditionalInfo string
	}{
		"1.2.3.4",
		"unknown dns",
		"unknown host name",
		"no additional info",
	}
	conn, _ := net.Dial("udp", "8.8.8.8:80")
	defer conn.Close()

	data.Ip = conn.LocalAddr().(*net.UDPAddr).String()
	data.Hostname, _ = os.Hostname()
	data.Dns = GetFQDN()

	tmpl.Execute(w,data)
}

//func serveHome(w http.ResponseWriter, r *http.Request) {
// 	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
//}

func main() {
	log.SetOutput(os.Stdout)
	log.SetOutput(os.Stdout)
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", serveWs)
	log.Fatal(http.ListenAndServe(*addr, nil))
	log.Println("started", addr)
}
