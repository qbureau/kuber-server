#!/bin/bash
#CGO_ENABLED=0 GOOS=linux go build -a -ldflags -o kuberloader '-extldflags "-static"' .
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/kuberloader kuberloadserver.go
cp home.html bin/
cp -R scripts bin/
#docker build -t andrey/kuberloader .
docker-compose down
docker-compose build
docker-compose up

# docker run --name=kuberloader -p 8080:8080 -d andrey/kuberloader