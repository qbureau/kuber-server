package commander

import (
	"log"
	"os"
	"fmt"
	"os/exec"
	"io"
	"bufio"
	"strconv"
)

func streamReader(stream io.ReadCloser, output chan<- string, done chan<- bool) {
	r := bufio.NewReader(stream)
	buf := make([]byte, 200)
	for {
		n, err := r.Read(buf)
		if n == 0 {
			if err == nil {
				continue
			}
			if err == io.EOF {
				log.Println("EOF stream reader")
				break
			}
			output <- err.Error()
			log.Fatal(err)
		}
		output <- string(buf[:n])
		log.Printf(string(buf))
		if err != nil && err != io.EOF {
			log.Fatal(err)
		}
	}
	done <- true
}

func RunScript(cmd []string, output chan<- string, done chan<- bool) {
	//ps -C stress-ng -o %cpu,%mem

	var res = ""

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(dir)
	cmd[0] = dir + "/scripts/" + cmd[0]+".sh"

	execCmd := exec.Command("/bin/sh", cmd...)

	stdout, err := execCmd.StdoutPipe()
	if err != nil {
		log.Print(err)
		res += err.Error() + "\n"
	}

	stderr, err := execCmd.StderrPipe()
	if err != nil {
		log.Print(err)
		res += err.Error() + "\n"
	}
	if err := execCmd.Start(); err != nil {
		log.Print(err)
		res += err.Error() + "\n"
	}

	doneOut := make(chan bool)
	doneErr := make(chan bool)
//	var wg sync.WaitGroup
	output<- "pid:["+strconv.FormatUint(uint64(execCmd.Process.Pid), 10)+"]\n"
	go streamReader(stdout, output, doneOut)
	go streamReader(stderr, output, doneErr)
	<-doneOut
	<-doneErr
	if err := execCmd.Wait(); err != nil {
		log.Print(err)
		res += err.Error() + "\n"
	}
	output <- res
	log.Print("The End")
	done <- true
}
