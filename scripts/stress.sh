#!/bin/bash
kill $(ps -C stress-ng-cpu --no-header -o pid)
kill $(ps -C stress-ng --no-header -o pid)
stress-ng -c 1 -l $1
